package source;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

public class Grafo {

	private LinkedList<Vertice>[] adj;
	private HashMap<String, LinkedList<Vertice>> grafo;
	private int[][] matriz;
	private Vertice[] vertices;
	private boolean[] visto;
	
	private enum Cor {
		BRANCO, CINZA, PRETO;
	}

	
	@SuppressWarnings("unchecked")
	public Grafo (int tamanho) {
		this.grafo = new HashMap<String, LinkedList<Vertice>>();
		this.adj = new LinkedList[tamanho];
		
		for (int i = 0; i < tamanho; i++) {
			this.adj[i] = new LinkedList<Vertice>();
		}
		
		this.matriz = new int[tamanho][tamanho];
		this.visto = new boolean[tamanho];
	}
	
	
	public String getNome (int x) {
		if (x < this.vertices.length && x >= 0)
			return this.vertices[x].getNome();
		
		else
			return "Identificador inv�lido.";
	}
	
	
	public LinkedList<Vertice> getAdjacencias (Vertice v) {
		return this.grafo.get(v.getNome());
	}
	
	
	public int[][] getMatriz () {
		return this.matriz;
	}
	
	
	public Vertice[] getVertices () {
		return this.vertices;
	}

	
	public boolean[] getVisto () {
		return this.visto;
	}
	
	
	public void setNome (int x, String nome) {
		if (x < this.vertices.length && x >= 0)
			this.vertices[x].setNome(nome);
		
		else
			System.out.println("V�rtice inv�lido.");;
	}
	
	
	public void setVisto (boolean[] visto) {
		this.visto = visto;
	}
	
	
//	public void addVertice (Vertice v) {
//		if (v != null)
//			this.grafo.put(v.getNome(), this.adj[v.getId()]);
//		else
//			System.out.println("V�rtice inv�lido.");
//	}
	
	
	public void addArco (Vertice x, Vertice y) {
		if (y != null) {
			this.adj[x.getId()].add(y);
			this.grafo.put(x.getNome(), this.adj[x.getId()]);
			this.matriz[x.getId()][y.getId()]++;
		}
		
		else
			System.out.println("V�rtice inv�lido.");
	}
	
	
	public void addAresta (Vertice x, Vertice y) {
		this.addArco(x, y);
		this.addArco(y, x);
		this.matriz[x.getId()][y.getId()]++;
		this.matriz[y.getId()][x.getId()]++;
	}
	
	
	public boolean testarArestaL (Vertice x, Vertice y) {
		if (x != null && y != null)
			return this.getAdjacencias(x).contains(y);
		
		else if (x == null)
			System.out.println("O primeiro v�rtice � inv�lido.");
		
		else
			System.out.println("O segundo v�rtice � inv�lido.");
		
		return false;
	}
	
	public boolean testarArestaM (Vertice x, Vertice y) {
		if (this.getMatriz()[x.getId()][y.getId()] != 0)
			return true;
		
		else
			return false;
	}
	

	public boolean testarLoopL (Grafo g) {
		if (g != null)
			for (Vertice x : g.getVertices()) {
				if (g.getAdjacencias(x).contains(x))
					return true;
			}
		
		else
			System.out.println("Grafo inv�lido.");
		
		return false;
	}
	
	
	public boolean testarLoopM (Grafo g) {
		for (int i = 0; i < g.getMatriz()[i].length - 1; i++) {
			if (g.getMatriz()[i][i] != 0)
				return true;
		}
		
		return false;
	}
	
	
	public HashMap<String, LinkedList<Vertice>> removerVertice (Vertice v) {
		HashMap<String, LinkedList<Vertice>> aux = this.grafo;
		
		aux.remove(v.getNome());
		
		return aux;
	}
	
	
	public void menorCaminho (Grafo g, Vertice origem) {
		Vertice[] antecessores = BFS(g, origem);
		
		for (Vertice x : g.getVertices())
			imprimir_caminho(origem, x, antecessores);
	}
	
	
	private Vertice[] BFS (Grafo g, Vertice origem) {
		LinkedList<Vertice> fila = new LinkedList<Vertice>();
		Vertice u;
		
		Cor[] cor = new Cor[g.grafo.size()];
		int[] distancia = new int[g.grafo.size()];
		Vertice[] ante = new Vertice[g.grafo.size()];
		
		for (Vertice x : g.getVertices()) {
			cor[x.getId()] = Cor.BRANCO;
			distancia[x.getId()] = -1;
			ante[x.getId()] = null;
		}
		
		cor[origem.getId()] = Cor.CINZA;
		distancia[origem.getId()] = 0; 
		
		fila.add(origem);

		while (!fila.isEmpty()) {
			u = fila.pop();

			for (Vertice v : g.adj[u.getId()]) {
				if (cor[v.getId()] == Cor.BRANCO) {
					cor[v.getId()] = Cor.CINZA;
					distancia[v.getId()] = distancia[u.getId()] + 1;
					ante[v.getId()] = u;
					fila.add(v);
				}
			}
			
			cor[u.getId()] = Cor.PRETO;
		}
		
		return ante;
	}
	
	
	private void imprimir_caminho (Vertice s, Vertice v, Vertice[] ante) {
		if (v.getId() == s.getId())
			System.out.print("\n" + s);
		
		else {
			if (ante[v.getId()] == null)
				System.out.println("N�o h� caminho.");
			
			else {
				imprimir_caminho(s, ante[v.getId()], ante);
				System.out.print(" -> " + v);
			}
		}
	}
	
	
//	public LinkedList<Vertice> encontrarCaminhoHamiltoniano (Grafo g, int tamanho) {
//		LinkedList<Vertice> lista = new LinkedList<Vertice>();
//		
//		if (tamanho == 2) {
//			if (g.testarArestaM(g.getVertices()[0], g.getVertices()[1])) {
//				lista.add(g.getVertices()[0]);
//				lista.add(g.getVertices()[1]);
//				
//				return lista;
//			}
//			
//			else {
//				lista.add(g.getVertices()[1]);
//				lista.add(g.getVertices()[0]);
//				
//				return lista;
//			}
//		}
//		
//		else {
//			Random  n = new Random();
//			Vertice x = g.getVertices()[n.nextInt(g.grafo.size())];
//			g.g
//			HashMap<String, LinkedList<Vertice>> gAux = g.removerVertice(x);
//			
//			encontrarCaminhoHamiltoniano(gAux, tamanho - 1);
//			
//			if (g.testarArestaL(x, g.getVertices()[0]))
//				lista.addFirst(x);
//			
//			if (g.testarArestaL(g.getVertices()[g.], y))
//		
//
//		}
//		
//		return null;
//	}
	
	
	/**
	 * Este m�todo recebe um vetor de v�rtices e a lista de caracteres lida do arquivo.
	 * Ent�o ele faz o split em cada linha do arquivo atribuindo o vetor obtido a um vetor
	 * de String denominado "aux".
	 * 
	 * O �ndice 0 desse vetor "aux" � ignorado pois � o nome do v�rtice.
	 * Do �ndice 1 ao N - 1 s�o as adjac�ncias lidas no arquivo.
	 * 
	 * O segundo for � utilizado para criar as adjac�ncias.
	 * Ele percorre os �ndices do vetor "aux" a partir do contador "k", parando no
	 * tamanho do vetor obtido pelo m�todo split para a linha no contador "i".
	 */
	public void preencher (Vertice[] v, ArrayList<String> caracteres) {
		String[] aux;
		this.vertices = v;
		
		if (caracteres != null) {
			for (int i = 1, j = 0; i < caracteres.size(); i++, j++) {
				aux = caracteres.get(i).split(" ");
				
				for (int k = 1; k < caracteres.get(i).split(" ").length; k++)
					this.addArco(v[j], v[Integer.parseInt(aux[k])]);
			}
		}
		
		else
			System.out.println("Lista inv�lida.");
	}
	
	
	public String toString () {		
		return this.grafo.toString();
	}


	
//	public boolean testarArcosParalelos (Grafo g) {
//	boolean result = false;
//	
//	if (g != null) {
//		Set<String> keys = g.grafo.keySet();
//		Collection<LinkedList<Vertice>> values = g.grafo.values();
//		
//		for (int i = 0; i < values.size(); i++)
//			if (values.toArray()[i].toString().contains(keys.toArray()[i].toString()))
//				result = true;
//	}
//	
//	else
//		System.out.println("Grafo inv�lido.");
//	
//	return result;
//}
//
//public Grafo transporGrafo (Grafo g) {
//	Grafo novo = new Grafo(g.grafo.size());
//	Vertice[] v = new Vertice[g.grafo.size()];
//	
//	Set<String> keys = g.grafo.keySet();
//	Collection<LinkedList<Vertice>> values = g.grafo.values();
//	
//	for (int i = 0; i < 5; i++) {
//		for (int j = 0, k = 0; j < 5; j++) {
//			if (values.toArray()[i].toString().indexOf(keys.toArray()[j].toString()) != -1) {
//				System.out.println(values.toArray()[i].toString() + " = " + keys.toArray()[j].toString());
//				
//				if (v[k] == null) {
//					for (int l = 0; l < k; l++) {
//						if (v[l].getNome() != keys.toArray()[j].toString()) {
//							v[k] = new Vertice(keys.toArray()[j].toString());
//							k++;
//						}
//					}
//				}
//			}
//		}
//	}
//	
//	return novo;
//
}