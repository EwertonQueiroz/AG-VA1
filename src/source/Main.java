package source;

public class Main {
	
	public static void main(String[] args) {
		
		Arquivo file = new Arquivo("graphHK");
		
		Grafo g = new Grafo(Arquivo.tamanho);
		Vertice[] v = new Vertice[Arquivo.tamanho];
		
		v = Vertice.criarVertices(file.getCaracteres(), Arquivo.tamanho);
		
		g.preencher(v, file.getCaracteres());
		
//		System.out.println(g.testarArestaM(v[0], v[1]));
//		System.out.println(g.testarArestaL(v[0], v[1]));
//		System.out.println(g.getNome(0));
		
//		g.setNome(0, "Teste");
//		System.out.println(g.getNome(0));
		
//		System.out.println(g.getAdjacencias(v[0]));
//		System.out.println(g.testarLoopM(g));
//		System.out.println(g.testarLoopL(g));
//		
//		System.out.println(g);
//		g.menorCaminho(g, v[0]);
//		
//		g.addArco(v[3], v[1]);
//		
//		System.out.println(g);
//		
//		System.out.println("");
//		
//		int[][] m = g.getMatriz();
//		
//		for (int i = 0; i < file.getTamanho(); i++) {
//			for (int j = 0; j < file.getTamanho(); j++)
//				System.out.print(m[i][j] + "\t");
//			System.out.println();
//		}
		
		HopcroftKarp hk = new HopcroftKarp();
		
//		graphHK
		Vertice[] a = new Vertice[4];
		a[0] = v[0]; a[1] = v[1]; a[2] = v[2]; a[3] = v[3];
		
		Vertice[] b = new Vertice[5];
		b[0] = v[4]; b[1] = v[5]; b[2] = v[6]; b[3] = v[7]; b[4] = v[8];
		g.getVisto()[4] = true; g.getVisto()[5] = true; g.getVisto()[6] = true;
		g.getVisto()[7] = true; g.getVisto()[8] = true; 
		
//		graphHK2
//		Vertice[] a = new Vertice[5];
//		a[0] = v[0]; a[1] = v[1]; a[2] = v[2]; a[3] = v[3]; a[4] = v[4];
//		
//		Vertice[] b = new Vertice[5];
//		b[0] = v[5]; b[1] = v[6]; b[2] = v[7]; b[3] = v[8]; b[4] = v[9];
//		g.getVisto()[5] = true; g.getVisto()[6] = true;
//		g.getVisto()[7] = true; g.getVisto()[8] = true; g.getVisto()[9] = true;

//		graphHK3
//		Vertice[] a = new Vertice[6];
//		a[0] = v[0]; a[1] = v[1]; a[2] = v[2]; a[3] = v[3]; a[4] = v[4]; a[5] = v[5];
//		
//		Vertice[] b = new Vertice[6];
//		b[0] = v[6]; b[1] = v[7]; b[2] = v[8]; b[3] = v[9]; b[4] = v[10]; b[5] = v[11];
//		g.getVisto()[6] = true; g.getVisto()[7] = true; g.getVisto()[8] = true;
//		g.getVisto()[9] = true; g.getVisto()[9] = true; g.getVisto()[10] = true;
//		g.getVisto()[11] = true;
		
//		System.out.println(hk.acharCaminhoAumentoHK(g, a, b));

//		System.out.println(hk.aumentarEmparelhamento(g, a, b, a[0]));

		hk.hopcroftKarp(g, a, b);
		
		
//		Em desenvolvimento...
//		System.out.println(g.transporGrafo(g));
	}

}
