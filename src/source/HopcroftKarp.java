package source;

import java.util.LinkedList;

public class HopcroftKarp {
	private int[] distancia = new int[Arquivo.tamanho];
	
	// Inicio s� pode ser elementos da parti��o A
	public boolean acharCaminhoAumento (Vertice inicio, Grafo g, Vertice[] partA, Vertice[] partB) {
		for (Vertice v : g.getAdjacencias(inicio)) {
			if (g.getVisto()[v.getId()] == false)
				g.getVisto()[v.getId()] = true;
			
			if (partB[v.getId() - Integer.min(partA.length, partB.length)] == v || acharCaminhoAumento (partB[v.getId() - Integer.min(partA.length, partB.length)], g, partA, partB)) {
				partA[inicio.getId()] = v;
				partB[v.getId() - Integer.min(partA.length, partB.length)] = inicio;
				
				return true;
			}
		}
		
		return false;
	}
	
	
	public boolean acharCaminhoAumentoHK (Grafo g, Vertice[] partA, Vertice[] partB) {
		LinkedList<Vertice> fila = new LinkedList<Vertice>();
		int k = Integer.MAX_VALUE;
		
		for (Vertice u : partA) {
			if (g.getVisto()[u.getId()])
				return false;
			else
				g.getVisto()[u.getId()] = true;
						
			
			if (partA[u.getId()] == u) {
				this.distancia[u.getId()] = 0;
				fila.add(u);
			}
		
			else
				this.distancia[u.getId()] = Integer.MAX_VALUE;
		}
		
		while (!fila.isEmpty()) {
			Vertice u = fila.pop();
			if (this.distancia[u.getId()] < k)
				for (Vertice v : g.getAdjacencias(u)) {
					if (v == partB[v.getId() - Integer.min(partA.length, partB.length)] && k == Integer.MAX_VALUE)
						k = this.distancia[u.getId()] + 1;
					
					else if (this.distancia[partB[v.getId() - Integer.min(partA.length, partB.length)].getId()] == Integer.MAX_VALUE) {
						this.distancia[partB[v.getId() - Integer.min(partA.length, partB.length)].getId()] = this.distancia[u.getId()] + 1;
						fila.add(partB[v.getId() - Integer.min(partA.length, partB.length)]);
					}
				}
		}
		
		return k != Integer.MAX_VALUE;
	}
	
	
	public boolean aumentarEmparelhamento (Grafo g, Vertice[] partA, Vertice[] partB, Vertice v) {
		for (Vertice u : g.getAdjacencias(v)) {
			if (partB[u.getId() - Integer.min(partA.length, partB.length)] == u) {
				partA[v.getId()] = u;
				partB[u.getId() - Integer.min(partA.length, partB.length)] = v;
				
				return true;
			}
			
			if (this.distancia[partB[u.getId() - 4].getId()] == this.distancia[v.getId()] + 1 && aumentarEmparelhamento(g, partA, partB, partB[u.getId() - 4])) {
				partA[v.getId()] = u;
				partB[u.getId() - Integer.min(partA.length, partB.length)] = v;
				
				return true;
			}
		}
		
		this.distancia[v.getId()] = Integer.MAX_VALUE;
		
		return false;
	}
	
	public void hopcroftKarp (Grafo g, Vertice[] partA, Vertice[] partB) {
		int emp = 0;
		
		while (acharCaminhoAumentoHK(g, partA, partB)) {
			for (Vertice v : partA) {
				if (partA[v.getId()] == v) {
					if (aumentarEmparelhamento(g, partA, partB, v))
						emp++;
				}
			}
		}
		
		for (Vertice v : partA)
			if (partB[v.getId() - Integer.min(partA.length, partB.length)] != v)
				System.out.println(v + " -> " + partB[v.getId() - 4]);
	}
}
